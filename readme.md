# Sistemas Distribuidos / Inteligência Artificial

Essa API tem como objetivo receber informações de estado sobre ar condicionados e luzes para definir se existe alguém dentro de uma sala de aula. Se não houver, a API deve mandar um email pedindo para desligar o ar ou a luz.  
Trabalho desenvolvido como parte das disciplinas de Sistemas Distribuidos e Inteligência Artificial da faculdade Anhanguera São José

## Dependencias

As bibliotecas a seguir precisam ser instaladas para funcionar corretamente

```python
pip install weather-api
pip install numpy
```

Essas outras bibliotecas também são utilizadas, porém, são bibliotecas padrão do python 2.7:

datetime  
urllib  
json  
smtplib

## Execução

```bash
python __main__.py
```

## Formato de JSON aceito

```json
{
	"Temperatura": [20, 25, 20, 20, 20],
	"datetime":"07/11/2018T12:23:00"
} 

{
	"Ilumi": [1,1,1,1,1,1,0,0,0,0,0,0],
	"datetime":"07/11/2018T12:23:00"
}
```

## Integrantes

Adler Pedro Borges de Carvalho  
Ariele Fátima das Dores - 214040811919  
Celso Leme dos Santos - 227106611919  
Eduardo Sbecker Reno - 222631511919  
Fabricio Gonçalves da Silva - 213991511919  
Janaína Lemes da Silva - 227067911919  
Jorge Luiz Acerbi Junior - 229865211919  
Lucas Braga  
Luiz Américo Brito Ferreira Silva - 223218611919  
Rafael Faria de Andrade - 227122211919  
Rafael Salvego dos Santos - 229508711919  
Vinicius Luchesi Montemor - 230123711919  
Vinicius Moraes Flores - 215055911919  
