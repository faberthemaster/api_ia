# -*- coding: utf-8 -*-
from datetime import datetime
from weather import Weather, Unit
import numpy

class ValidaDados:
	
	temperaturaAtual = 0
	temperaturaExterna = 0
	luzLigada = 1
	data_temperatura = "01/01/2018T00:00:00" #dia/mes/ano hora:minuto:segundo
	estacao = "Verao"

	def __init__(self, temperaturaAtual, data_temperatura, luzLigada):
		self.temperaturaAtual = numpy.mean(temperaturaAtual)
		self.temperaturaExterna = self.obterTemperaturaExternaAtual()
		self.data_temperatura = self.converteParaData(data_temperatura)
		self.estacao = self.obterEstacaoComBaseEmData()
		self.luzLigada = 1 if numpy.mean(luzLigada) > 0 else 0
		self.mostraInformacoes()

	def inicializaTemperaturas(self):
		dados_estacao = (["Primavera",20,24],
						["Verao",18,22],
						["Outono",21,25],
						["Inverno",22,26])
		return dados_estacao

	def arEstaLigado(self):
		for status_tempo in self.inicializaTemperaturas():
			if (self.estacao == status_tempo[0] and (status_tempo[1] <= self.temperaturaAtual <= status_tempo[2])) or (self.temperaturaExterna - self.temperaturaAtual >= 5):
				print "Estado do ar (1 ligado e 0 desligado): ....... 1"
				return True
		print "Estado do ar (1 ligado e 0 desligado): ....... 0"

	def deveEnviarEmail(self):
		if (self.arEstaLigado() or self.luzLigada): 
			if (self.data_temperatura.weekday() == 6):
				print "Domingo, deve enviar email"
				return True
			elif (self.data_temperatura.weekday() == 5) and ((self.data_temperatura.hour < 7) or (self.data_temperatura.hour > 12)):
				print "Sabado fora do horario de aula (7:00 ate 12:00), deve enviar email"
				return True
			elif ((int(self.data_temperatura.hour) < 7) or (int(self.data_temperatura.hour) > 22)):  
				print "Segunda a sexta fora do horario de aula (7:00 ate 22:00), deve enviar email"
				return True
			else:
				print "Dentro do horario de aula, nao deve enviar email"
				return False
		else:
			print "O ar e luz estao desligados, nao deve enviar email"
			return False

	def converteParaData(self, string_data):
		data_hora = datetime.strptime(string_data, "%d/%m/%YT%H:%M:%S")
		return data_hora

	def obterEstacaoComBaseEmData(self):
		dia_do_ano = self.data_temperatura.timetuple().tm_yday
		outono = range(80, 172)
		inverno = range(173, 264)
		primavera = range(265, 355)

		if dia_do_ano in outono:
			return "Outono"
		elif dia_do_ano in inverno:
			return "Inverno"
		elif dia_do_ano in primavera:
			return "Primavera"
		else:
			return "Verao"

	def mostraInformacoes(self):
		print "Temperatura media atual: ..................... {0} graus".format(self.temperaturaAtual)
		print "Temperatura media externa: ................... {0} graus".format(self.temperaturaExterna)
		print "Data e hora da temperatura: .................. {0}".format(self.data_temperatura)
		print "Estacao do ano: .............................. {0}".format(self.estacao)
		print "Estado da luz (1 = ligado e 0 = desligado): .. {0}".format(self.luzLigada)

	def obterTemperaturaExternaAtual(self):
		weather = Weather(unit=Unit.CELSIUS)
		lookup = weather.lookup(455912)
		condition = lookup.condition
		return int(condition.temp)

if __name__ == "__main__":
	objName = ValidaDados()
	objName.inicializaTemperaturas()