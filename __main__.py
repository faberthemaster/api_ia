# -*- coding: utf-8 -*-
from MandarEmail import *
from ValidaDados import *
from LerJSON import *

class MainClass:
	def main(self):
		# JSON pode ler tanto de um endereço local quanto remoto
		json_temp = LerJSON('C:/Users/fsilva66/Desktop/python/temp.json')
		json_ilum = LerJSON('C:/Users/fsilva66/Desktop/python/ilumi.json')
		json_temp_data = json_temp.processarJSON()
		json_ilum_data = json_ilum.processarJSON()
		validaDados = ValidaDados(json_temp_data["Temperatura"],json_temp_data["datetime"],json_ilum_data["Ilumi"])
		MandarEmail(validaDados.deveEnviarEmail())

if __name__ == "__main__":
	objName = MainClass()
	objName.main()