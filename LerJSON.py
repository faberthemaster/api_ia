# -*- coding: utf-8 -*-
import urllib, json

class LerJSON:

	caminhoJson = ""

	def main(self):
		pass

	def __init__(self, caminhoJson):
		if caminhoJson == "":
			print "O caminho {0} para o JSON é inválido.".format(caminhoJson)
		else:
			self.caminhoJson = caminhoJson

	def processarJSON(self):
		print "Processando JSON: ............................ {0}".format(self.caminhoJson)
		
		try:
			with open(self.caminhoJson) as parseJSON:
				try:
					data = json.load(parseJSON)
					return data
				except ValueError:
					print "JSON Invalido: {0}".format(self.caminhoJson)
					exit()
		except:
			try:
				response = urllib.urlopen(self.caminhoJson)
				data = json.loads(response.read())
				return data
			except ValueError:
				print "JSON Invalido: {0}".format(self.caminhoJson)
				exit()

if __name__ == "__main__":
	objName = MandarEmail()
	objName.main()